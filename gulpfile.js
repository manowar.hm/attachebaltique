var gulp = require('gulp');
var scss = require('gulp-sass');
var icomoonConverter = require('gulp-icomoon-converter');
var minimatch = require('minimatch');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var unzip = require('gulp-unzip');
var connect = require('gulp-connect');
var twig = require('gulp-twig');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var importer = require('node-sass-importer');


// Example Gulp task to demonstrate how plugin can be used to convert icomoon.io project and fonts distributive
// into SCSS configuration file with list of icons and set of font files inside project's assets directory
gulp.task('font-icons', function (cb) {
    // There is number of configuration variables to define extensibility points
    // It is not needed to keep them in this way

    var icomoonProjectFile = 'app/fonts/attachebaltique.json';   // путь к файлу проекта созданного icomoon
    var icomoonDistributive = 'app/fonts/attachebaltique.zip';  // zip-архив сгененирированный icomoon
    var iconFontsFileFormats = ['woff', 'ttf'];   //список форматов файлов
    var fontFilesTargetName = 'icons'; // целевое имя для файлов шрифтов
    var fontFilesTargetPath = 'app/fonts/';  // целевой путь для файлов шрифтов с иконками
    var iconsInfoTargetPath = 'info';   // Target path to store file with information about font icons to

    series([
        function (next) {
            // Unpack icomoon.io distributive file, extract font files of requested formats
            // and put them into project's assets directory under required name
            gulp.src(icomoonDistributive)
                .pipe(plumber())
                .pipe(unzip({
                    filter: function (entry) {
                        // We only need font files to be extracted from distributive and only into defined formats
                        return minimatch(entry.path, 'fonts/*.+(' + iconFontsFileFormats.join('|') + ')');
                    }
                }))
                .pipe(rename({
                    dirname: '',        // Remove "fonts" directory path that is available into distributive
                    basename: fontFilesTargetName
                }))
                .pipe(gulp.dest(fontFilesTargetPath))
                .on('error', function () {
                    console.log(['icomoon.io converter: icons extraction failed', arguments])
                })
                .on('end', next);
        },
        function (next) {
            // Generate file with font icons information
            gulp.src(icomoonProjectFile)
                .pipe(plumber())
                .pipe(icomoonConverter({
                    // You can add various options here, refer documentation for details
                }))
                .pipe(gulp.dest(iconsInfoTargetPath))
                .on('error', function () {
                    console.log(['icomoon.io converter: icons information generator failed', arguments])
                })
                .on('end', next);
        }
    ], cb);
});


gulp.task('scss', function () {
    return gulp.src('app/scss/*.scss')
        .pipe(plumber())
        .pipe(scss().on('error', scss.logError))
        .pipe(gulp.dest('app/css'));
});

gulp.task('css', ['scss'], function () {
    return gulp.src('app/css/*.css')
        .pipe(plumber())
        .pipe(connect.reload());
});

gulp.task('connect', function () {
    connect.server({
        root: '',
        livereload: true
    });
});

gulp.task('html', function () {
    gulp.src('*.html')
        .pipe(connect.reload());
});

gulp.task('watch', ['connect'], function () {
    gulp.watch('*.html', ['html']);
    gulp.watch('app/scss/*.scss', ['css'])
});

gulp.task('default', ['css', 'html']);



